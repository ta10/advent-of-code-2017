from collections import defaultdict

instr = {"A":[(1,1,"B"),(0,-1,"E")], "B":[(1,-1,"C"),(0,1,"A")], "C":[(1,-1,"D"),(0,1,"C")], "D":[(1,-1,"E"),(0,-1,"F")], "E":[(1,-1,"A"),(1,-1,"C")], "F":[(1,-1,"E"),(1,1,"A")]}
tape = defaultdict(int)
state = "A"
pos = 0

for _ in range(12386363):
    c = instr[state][tape[pos]]
    tape[pos] = c[0]
    pos += c[1]
    state = c[2]

print("Part 1:", sum(tape.values()))